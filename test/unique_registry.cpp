#include <depi/registry.hpp>

namespace jaf::depi::test::unique_registry
{
    struct global
    {
        MOCK_METHOD(void, f, (), ());
        MOCK_METHOD(void, g, (), ());
        MOCK_METHOD(void, h, (), ());
    };

    std::unique_ptr<global> glob = nullptr;

    struct unique_registry
        : ::jaf::testing::test_suite
    {
        void SetUp() final
        {
            glob = std::make_unique<global>();
        }

        void TearDown() final
        {
            glob.reset();
        }
        
        jaf::depi::registry r_;
    };

    struct inf1
    {
        virtual void f() = 0;

        virtual ~inf1() = default;
    };

    struct inf2
    {
        virtual void g() = 0;

        virtual ~inf2() = default;
    };

    struct inf3
        : inf1
    {
        void f() override
        {
            h();
        }

        virtual void h() = 0;

        virtual ~inf3() = default;
    };

    struct impl1
        : inf1
    {
        void f() override
        {
            glob->f();
        }
    };

    struct impl2
        : inf2
    {
        impl2(std::unique_ptr<inf1> ptr)
        {
            if(ptr != nullptr)
            {
                ptr->f();
            }
        }

        void g() override
        {
            glob->g();
        }
    };
    
    struct impl3
        : inf1
    {
        void f() override
        {
            glob->f();
        }
    };

    struct impl4
        : inf2
    {
        void g() override
        {
            glob->g();
        }
    };

    struct impl5
        : inf3
    {
        void h() override
        {
            glob->h();
        }
    };

    TEST_F(unique_registry, basic_bind_type)
    {
        r_.bind<jaf::depi::unique_registry::tag, inf1, impl1>();

        auto i1 = r_.create<std::unique_ptr<inf1>>();
    }

    TEST_F(unique_registry, basic_inject_type)
    {
        EXPECT_CALL(*glob, f()).Times(1);

        r_.bind<jaf::depi::unique_registry::tag, inf1, impl1>();
        r_.bind<jaf::depi::unique_registry::tag, inf2, impl2>();

        auto i2 = r_.create<std::unique_ptr<inf2>>();
    }

    TEST_F(unique_registry, basic_bind_alias)
    {
        r_.bind<jaf::depi::unique_registry::tag, inf1, inf3>();
        r_.bind<jaf::depi::unique_registry::tag, inf3, impl5>();

        EXPECT_CALL(*glob, h()).Times(1);

        auto i2 = r_.create<std::unique_ptr<inf1>>();
        i2->f();
    }

    TEST_F(unique_registry, basic_bind_factory)
    {
        const auto f = [](){ return std::make_unique<impl1>(); };

        r_.bind<jaf::depi::unique_registry::tag, inf1>(f);

        auto i1 = r_.create<std::unique_ptr<inf1>>();
    }

    TEST_F(unique_registry, basic_inject_factory_1)
    {
        const auto f = [](){ return std::make_unique<impl1>(); };

        r_.bind<jaf::depi::unique_registry::tag, inf1>(f);
        r_.bind<jaf::depi::unique_registry::tag, inf2, impl2>();

        auto i2 = r_.create<std::unique_ptr<inf2>>();
    }

    TEST_F(unique_registry, basic_inject_factory_2)
    {
        const auto f = [](){ return std::make_unique<impl2>(nullptr); };

        r_.bind<jaf::depi::unique_registry::tag, inf1, impl1>();
        r_.bind<jaf::depi::unique_registry::tag, inf2>(f);

        EXPECT_CALL(*glob, f()).Times(0);

        auto i2 = r_.create<std::unique_ptr<inf2>>();
    }

    TEST_F(unique_registry, double_bind_type)
    {
        r_.bind<jaf::depi::unique_registry::tag, inf1, impl1>();

        const auto proxy = [&]()
        {
            r_.bind<jaf::depi::unique_registry::tag, inf1, impl3>();
        };

        EXPECT_THROW({
            proxy();
        }, jaf::depi::exceptions::bind_error);
    }
    
    TEST_F(unique_registry, double_bind_factory)
    {
        r_.bind<jaf::depi::unique_registry::tag, inf1, impl1>();

        const auto proxy = [&]()
        {
            auto f = [](){ return std::make_unique<impl3>(); };
            r_.bind<jaf::depi::unique_registry::tag, inf1>(f);
        };

        EXPECT_THROW({
            proxy();
        }, jaf::depi::exceptions::bind_error);
    }
}
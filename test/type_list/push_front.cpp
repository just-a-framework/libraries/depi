#include <depi/type_list/push_front.hpp>

namespace jaf::depi::test::push_front
{
    struct push_front
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(push_front, builtin)
    {
        using l = jaf::depi::type_list<bool, float, uint8_t>;
        const auto t = std::is_same_v<jaf::depi::type_list<int, bool, float, uint8_t>, jaf::depi::push_front_t<int, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    struct foo
    {
    };

    struct bar
    {
    };

    struct foobar
    {
    };
    
    TEST_F(push_front, userdefined)
    {
        using l = jaf::depi::type_list<foobar, foo, foobar>;
        const auto t = std::is_same_v<jaf::depi::type_list<bar, foobar, foo, foobar>, jaf::depi::push_front_t<bar, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
    
    TEST_F(push_front, mixed1)
    {
        using l = jaf::depi::type_list<int, float, foo, bar>;
        const auto t = std::is_same_v<jaf::depi::type_list<foobar, int, float, foo, bar>, jaf::depi::push_front_t<foobar, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(push_front, mixed2)
    {
        using l = jaf::depi::type_list<bar, foobar, int, foo, bool>;
        const auto t = std::is_same_v<jaf::depi::type_list<float, bar, foobar, int, foo, bool>, jaf::depi::push_front_t<float, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(push_front, one)
    {
        using l = jaf::depi::type_list<>;
        const auto t = std::is_same_v<jaf::depi::type_list<float>, jaf::depi::push_front_t<float, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
}
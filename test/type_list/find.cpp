#include <depi/type_list/find.hpp>

namespace jaf::depi::test::find
{
    template<class T>
    struct is_int
        : std::is_same<T, int>
    {
    };

    struct find
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(find, builtin_found)
    {
        using l = jaf::depi::type_list<float, uint8_t, int, bool>;
        const auto t = std::is_same_v<int, jaf::depi::find_if_t<is_int, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(find, builtin_not_found)
    {
        using l = jaf::depi::type_list<float, uint8_t, bool>;

        {
            const auto t = std::is_same_v<int, jaf::depi::find_if_t<is_int, l>>;
            static_assert(!t);
            EXPECT_TRUE(!t);
        }
        {
            const auto t = std::is_same_v<jaf::depi::none, jaf::depi::find_if_t<is_int, l>>;
            static_assert(t);
            EXPECT_TRUE(t);
        }
    }

    struct foo
    {
    };

    struct bar
    {
    };

    struct foobar
        : foo
        , bar
    {
    };

    template<class T>
    struct is_bar
        : std::is_same<T, bar>
    {
    };

    TEST_F(find, userdefined_found)
    {
        using l = jaf::depi::type_list<foo, bar, foobar, bar>;
        const auto t = std::is_same_v<bar, jaf::depi::find_if_t<is_bar, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(find, userdefined_not_found)
    {
        using l = jaf::depi::type_list<foo, foo, foobar>;

        {
            const auto t = std::is_same_v<bar, jaf::depi::find_if_t<is_bar, l>>;
            static_assert(!t);
            EXPECT_TRUE(!t);
        }
        {
            const auto t = std::is_same_v<jaf::depi::none, jaf::depi::find_if_t<is_bar, l>>;
            static_assert(t);
            EXPECT_TRUE(t);
        }
    }
}
#include <depi/type_list/push_back.hpp>

namespace jaf::depi::test::push_back
{
    struct push_back
        : ::jaf::testing::test_suite
    {
    };
    
    TEST_F(push_back, builtin)
    {
        using l = jaf::depi::type_list<bool, float, uint8_t>;
        const auto t = std::is_same_v<jaf::depi::type_list<bool, float, uint8_t, int>, jaf::depi::push_back_t<int, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    struct foo
    {
    };

    struct bar
    {
    };

    struct foobar
    {
    };
    
    TEST_F(push_back, userdefined)
    {
        using l = jaf::depi::type_list<foobar, foo, foobar>;
        const auto t = std::is_same_v<jaf::depi::type_list<foobar, foo, foobar, bar>, jaf::depi::push_back_t<bar, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
    
    TEST_F(push_back, mixed1)
    {
        using l = jaf::depi::type_list<int, float, foo, bar>;
        const auto t = std::is_same_v<jaf::depi::type_list<int, float, foo, bar, foobar>, jaf::depi::push_back_t<foobar, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(push_back, mixed2)
    {
        using l = jaf::depi::type_list<bar, foobar, int, foo, bool>;
        const auto t = std::is_same_v<jaf::depi::type_list<bar, foobar, int, foo, bool, float>, jaf::depi::push_back_t<float, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(push_back, one)
    {
        using l = jaf::depi::type_list<>;
        const auto t = std::is_same_v<jaf::depi::type_list<float>, jaf::depi::push_back_t<float, l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
}
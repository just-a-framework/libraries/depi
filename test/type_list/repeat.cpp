#include <depi/type_list/repeat.hpp>

namespace jaf::depi::test::repeat
{
    struct repeat
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(repeat, builtin)
    {
        using l = jaf::depi::type_list<uint8_t, uint8_t, uint8_t>;
        const auto t = std::is_same_v<l, jaf::depi::repeat_t<uint8_t, 3>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    struct foo
    {
    };
    
    TEST_F(repeat, userdefined)
    {
        using l = jaf::depi::type_list<foo, foo, foo, foo>;
        const auto t = std::is_same_v<l, jaf::depi::repeat_t<foo, 4>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(repeat, one)
    {
        using l = jaf::depi::type_list<float>;
        const auto t = std::is_same_v<l, jaf::depi::repeat_t<float, 1>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(repeat, zero)
    {
        using l = jaf::depi::type_list<>;
        const auto t = std::is_same_v<l, jaf::depi::repeat_t<float, 0>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
}
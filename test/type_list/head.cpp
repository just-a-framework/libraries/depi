#include <depi/type_list/head.hpp>

namespace jaf::depi::test::head
{
    struct head
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(head, builtin)
    {
        using l = jaf::depi::type_list<int, bool, float, uint8_t>;
        const auto t = std::is_same_v<int, jaf::depi::head_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    struct foo
    {
    };

    struct bar
    {
    };

    struct foobar
    {
    };
    
    TEST_F(head, userdefined)
    {
        using l = jaf::depi::type_list<bar, foobar, foo, foobar>;
        const auto t = std::is_same_v<bar, jaf::depi::head_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
    
    TEST_F(head, mixed1)
    {
        using l = jaf::depi::type_list<foobar, int, float, foo, bar>;
        const auto t = std::is_same_v<foobar, jaf::depi::head_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(head, mixed2)
    {
        using l = jaf::depi::type_list<float, bar, foobar, int, foo>;
        const auto t = std::is_same_v<float, jaf::depi::head_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(head, one)
    {
        using l = jaf::depi::type_list<float>;
        const auto t = std::is_same_v<float, jaf::depi::head_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
}
#include <depi/type_list/apply.hpp>

namespace jaf::depi::test::apply
{
    struct foo
    {
    };

    struct bar
    {
    };

    struct foobar
    {
        foobar(foo, bar)
        {
        }
    };

    struct apply
        : ::jaf::testing::test_suite
    {
    };

    TEST_F(apply, is_constructible)
    {
        const auto v = jaf::depi::apply_v<
            std::is_constructible,
            jaf::depi::type_list<
                foobar, foo, bar
            >
        >;
        static_assert(v);
        EXPECT_TRUE(v);
    }

    TEST_F(apply, isnt_constructible)
    {
        const auto v = jaf::depi::apply_v<
            std::is_constructible,
            jaf::depi::type_list<
                foobar, bar, bar
            >
        >;
        static_assert(!v);
        EXPECT_TRUE(!v);
    }
}
#include <depi/type_list/tail.hpp>

namespace jaf::depi::test::tail
{
    struct tail
        : ::jaf::testing::test_suite
    {
    };
    
    TEST_F(tail, builtin)
    {
        using l = jaf::depi::type_list<int, bool, float, uint8_t>;
        const auto t = std::is_same_v<jaf::depi::type_list<bool, float, uint8_t>, jaf::depi::tail_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    struct foo
    {
    };

    struct bar
    {
    };

    struct foobar
    {
    };
    
    TEST_F(tail, userdefined)
    {
        using l = jaf::depi::type_list<bar, foobar, foo, foobar>;
        const auto t = std::is_same_v<jaf::depi::type_list<foobar, foo, foobar>, jaf::depi::tail_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
    
    TEST_F(tail, mixed1)
    {
        using l = jaf::depi::type_list<foobar, int, float, foo, bar>;
        const auto t = std::is_same_v<jaf::depi::type_list<int, float, foo, bar>, jaf::depi::tail_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(tail, mixed2)
    {
        using l = jaf::depi::type_list<float, bar, foobar, int, foo, bool>;
        const auto t = std::is_same_v<jaf::depi::type_list<bar, foobar, int, foo, bool>, jaf::depi::tail_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }

    TEST_F(tail, one)
    {
        using l = jaf::depi::type_list<float>;
        const auto t = std::is_same_v<jaf::depi::type_list<>, jaf::depi::tail_t<l>>;
        static_assert(t);
        EXPECT_TRUE(t);
    }
}
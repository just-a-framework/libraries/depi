#include <depi/locker.hpp>

namespace jaf::depi::test::locker
{
    struct locker
        : ::jaf::testing::test_suite
    {
        jaf::depi::locker l_;
    };

    TEST_F(locker, one_lock_unlock)
    {
        l_.lock(typeid(int));
        l_.unlock(typeid(int));
    }

    TEST_F(locker, multiple_lock_unlock)
    {
        l_.lock(typeid(int));
        l_.lock(typeid(float));
        l_.unlock(typeid(int));
        l_.unlock(typeid(float));
    }

    TEST_F(locker, guard_one_lock_unlock)
    {
        const auto g = l_.lock<int>();
    }

    TEST_F(locker, guard_multiple_lock_unlock)
    {
        const auto g1 = l_.lock<int>();
        const auto g2 = l_.lock<float>();
    }

    TEST_F(locker, repeat_lock)
    {
        EXPECT_NO_THROW({
            l_.lock(typeid(int));
            l_.lock(typeid(float));
        });

        EXPECT_THROW({
            l_.lock(typeid(int));
        }, jaf::depi::exceptions::lock_error);
    }

    TEST_F(locker, guard_repeat_lock)
    {
        EXPECT_THROW({
            const auto g2 = l_.lock<int>();
            const auto g1 = l_.lock<int>();
        }, jaf::depi::exceptions::lock_error);

        
        EXPECT_NO_THROW({
            const auto g2 = l_.lock<int>();
            const auto g1 = l_.lock<float>();
        });
    }
    
    TEST_F(locker, unmatched_unlock)
    {
        l_.lock(typeid(int));
        EXPECT_THROW({
            l_.unlock(typeid(float));
        }, jaf::depi::exceptions::unlock_error);
    }
}
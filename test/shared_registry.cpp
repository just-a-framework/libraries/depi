#include <depi/registry.hpp>

namespace jaf::depi::test::shared_registry
{
    struct shared_registry
        : ::jaf::testing::test_suite
    {
        jaf::depi::registry r_;
    };

    struct inf1
    {
        virtual void f() = 0;

        virtual ~inf1() = default;
    };

    struct inf2
    {
        virtual void g() = 0;

        virtual ~inf2() = default;
    };

    struct inf3 : inf1
    {
        void f() override
        {
            h();
        }

        virtual void h() = 0;

        virtual ~inf3() = default;
    };

    struct impl1 : inf1
    {
        MOCK_METHOD(void, f, (), ());
    };

    struct impl2 : inf2
    {
        impl2(std::shared_ptr<inf1> ptr)
        {
            if(ptr != nullptr)
            {
                ptr->f();
            }
        }

        MOCK_METHOD(void, g, (), ());
    };
    
    struct impl3 : inf1
    {
        MOCK_METHOD(void, f, (), ());
    };

    struct impl4 : inf2
    {
        MOCK_METHOD(void, f, (), ());
    };

    struct impl5 : inf3
    {
        MOCK_METHOD(void, h, (), ());
    };

    TEST_F(shared_registry, basic_bind_type)
    {
        r_.bind<jaf::depi::shared_registry::tag, inf1, impl1>();

        auto i1 = r_.create<std::shared_ptr<inf1>>();
    }

    TEST_F(shared_registry, basic_inject_type)
    {
        r_.bind<jaf::depi::shared_registry::tag, inf1, impl1>();
        r_.bind<jaf::depi::shared_registry::tag, inf2, impl2>();

        auto i1 = r_.create<std::shared_ptr<impl1>>();

        EXPECT_CALL(*(i1.get()), f()).Times(1);

        auto i2 = r_.create<std::shared_ptr<inf2>>();
    }

    TEST_F(shared_registry, basic_bind_alias)
    {
        r_.bind<jaf::depi::shared_registry::tag, inf1, inf3>();
        r_.bind<jaf::depi::shared_registry::tag, inf3, impl5>();

        auto i1 = r_.create<std::shared_ptr<impl5>>();

        EXPECT_CALL(*(i1.get()), h()).Times(1);

        auto i2 = r_.create<std::shared_ptr<inf1>>();
        i2->f();
    }

    TEST_F(shared_registry, basic_bind_object)
    {
        auto p = std::make_shared<impl1>();

        r_.bind<jaf::depi::shared_registry::tag, inf1>(p);

        auto i1 = r_.create<std::shared_ptr<inf1>>();
    }

    TEST_F(shared_registry, basic_inject_object)
    {
        auto p = std::make_shared<impl1>();

        r_.bind<jaf::depi::shared_registry::tag, inf1>(p);
        r_.bind<jaf::depi::shared_registry::tag, inf2, impl2>();

        EXPECT_CALL(*(p.get()), f()).Times(1);

        auto i2 = r_.create<std::shared_ptr<inf2>>();
    }

    TEST_F(shared_registry, basic_bind_factory)
    {
        const auto f = [](){ return std::make_shared<impl1>(); };

        r_.bind<jaf::depi::shared_registry::tag, inf1>(f);

        auto i1 = r_.create<std::shared_ptr<inf1>>();
    }

    TEST_F(shared_registry, basic_inject_factory_1)
    {
        auto p = std::make_shared<impl1>();

        const auto f = [&](){ return p; };

        r_.bind<jaf::depi::shared_registry::tag, inf1>(f);
        r_.bind<jaf::depi::shared_registry::tag, inf2, impl2>();

        EXPECT_CALL(*(p.get()), f()).Times(1);

        auto i2 = r_.create<std::shared_ptr<inf2>>();
    }

    TEST_F(shared_registry, basic_inject_factory_2)
    {
        const auto f = [](){ return std::make_shared<impl2>(nullptr); };

        r_.bind<jaf::depi::shared_registry::tag, inf1, impl1>();
        r_.bind<jaf::depi::shared_registry::tag, inf2>(f);

        auto i1 = r_.create<std::shared_ptr<impl1>>();

        EXPECT_CALL(*(i1.get()), f()).Times(0);

        auto i2 = r_.create<std::shared_ptr<inf2>>();
    }

    TEST_F(shared_registry, double_bind_type)
    {
        r_.bind<jaf::depi::shared_registry::tag, inf1, impl1>();

        const auto proxy = [&]()
        {
            r_.bind<jaf::depi::shared_registry::tag, inf1, impl3>();
        };

        EXPECT_THROW({
            proxy();
        }, jaf::depi::exceptions::bind_error);
    }

    TEST_F(shared_registry, double_bind_object)
    {
        r_.bind<jaf::depi::shared_registry::tag, inf1, impl1>();

        const auto proxy = [&]()
        {
            auto p = std::make_shared<impl3>();
            r_.bind<jaf::depi::shared_registry::tag, inf1>(p);
        };

        EXPECT_THROW({
            proxy();
        }, jaf::depi::exceptions::bind_error);
    }
    
    TEST_F(shared_registry, double_bind_factory)
    {
        r_.bind<jaf::depi::shared_registry::tag, inf1, impl1>();

        const auto proxy = [&]()
        {
            auto f = [](){ return std::make_shared<impl3>(); };
            r_.bind<jaf::depi::shared_registry::tag, inf1>(f);
        };

        EXPECT_THROW({
            proxy();
        }, jaf::depi::exceptions::bind_error);
    }
}
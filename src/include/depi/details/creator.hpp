#pragma once

#include <depi/registry_fwd.hpp>
#include <depi/details/registry_except.hpp>
#include <depi/details/constructor_param_count.hpp>

namespace jaf::depi::details
{
    template<class T, template<class> class U>
    struct creator
    {
        using type = typename U<T>::type;

        static type make(registry& r)
        {
            constexpr auto param_count = details::constructor_param_count<T>::value;
            return make_proxy(r, std::make_index_sequence<param_count>());
        }

        template<class... Args>
        static type make(registry& r, std::function<type(Args...)> f)
        {
            constexpr auto param_count = sizeof...(Args);
            return make_proxy(r, f, std::make_index_sequence<param_count>());
        }

    private:
        template<std::size_t... Ind>
        static type make_proxy(registry& r, std::index_sequence<Ind...>)
        {
            const auto get = [&r](std::size_t) { return details::registry_except<T>{ r }; };
            return U<T>::make(get(Ind)...);
        }

        template<std::size_t... Ind, class Func>
        static type make_proxy(registry& r, Func f, std::index_sequence<Ind...>)
        {
            const auto get = [&r](std::size_t) { return details::registry_except<T>{ r }; };
            return f(get(Ind)...);
        }
    };
}
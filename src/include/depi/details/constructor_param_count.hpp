#pragma once

#include <depi/type_list/type_list.hpp>
#include <depi/type_list/apply.hpp>
#include <depi/type_list/push_front.hpp>
#include <depi/type_list/repeat.hpp>
#include <depi/details/any_except.hpp>

namespace jaf::depi::details
{
    template<class T>
    struct failure
    {
        static_assert(!std::is_same_v<T, T>, "Cannot deduce constructor argument count!");
    };

    inline constexpr std::size_t max_constructor_parameter = 10;

    template<class T, std::size_t Current = max_constructor_parameter>
    struct constructor_param_count
        : std::conditional_t<
            apply_v<
                std::is_constructible,
                push_front_t<
                    T,
                    repeat_t<any_except<T>, Current>
                >
            >,
            std::integral_constant<std::size_t, Current>,
            constructor_param_count<T, Current - 1>
        >
    {
    };

    template<class T>
    struct constructor_param_count<T, 0>
        : std::conditional_t<
            apply_v<
                std::is_constructible,
                push_front_t<
                    T,
                    repeat_t<any_except<T>, 0>
                >
            >,
            std::integral_constant<std::size_t, 0>,
            failure<T>
        >
    {
    };
}

#pragma once

#include <depi/registry_fwd.hpp>

namespace jaf::depi::details
{
    template<class T>
    struct registry_except
    {
        registry_except(registry& r)
            : r_{ r }
        {
        }

        template<
            class U,
            class = std::enable_if_t<
                !std::is_same_v<
                    std::remove_cv_t<T>,
                    std::remove_cv_t<U>
                >
            >
        >
        operator U() const
        {
            return r_;
        }

        template<
            class U,
            class = std::enable_if_t<
                !std::is_same_v<
                    std::remove_cv_t<T>,
                    std::remove_cv_t<U>
                >
            >
        >
        operator U()
        {
            return r_;
        }

    private:
        registry& r_;
    };
}

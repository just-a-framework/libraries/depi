#pragma once

namespace jaf::depi::details
{
    template<template<class...> class Template, class T>
    struct is_instance_of
        : std::false_type
    {
    };

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, const T>
        : is_instance_of<Template, T>
    {
    };

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, volatile T>
        : is_instance_of<Template, T>
    {
    };

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, T&>
        : is_instance_of<Template, T>
    {
    };

    template<template<class...> class Template, class T>
    struct is_instance_of<Template, T&&>
        : is_instance_of<Template, T>
    {
    };

    template<template<class...> class Template, class... Args>
    struct is_instance_of<Template, Template<Args...>>
        : std::true_type
    {
    };
}

#pragma once

namespace jaf::depi::details
{
    template<class T>
    struct any_except
    {
        template<
            class U,
            class = std::enable_if_t<
                !std::is_same_v<
                    std::remove_cv_t<T>,
                    std::remove_cv_t<U>
                >
            >
        >
        operator U() const;

        template<
            class U,
            class = std::enable_if_t<
                !std::is_same_v<
                    std::remove_cv_t<T>,
                    std::remove_cv_t<U>
                >
            >
        >
        operator U();
    };
}

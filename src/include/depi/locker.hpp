#pragma once

#include <depi/exceptions/lock_error.hpp>
#include <depi/exceptions/unlock_error.hpp>

namespace jaf::depi
{
    struct locker
    {
        struct guard
        {
            guard(std::type_index t, locker& l)
                : t_{ std::move(t) }
                , l_{ l }
            {
                l_.lock(t_);
            }

            ~guard()
            {
                l_.unlock(t_);
            }
        private:
            const std::type_index t_;
            locker& l_;
        };

        template<class T>
        guard lock()
        {
            return guard{ typeid(T), *this };
        }

        void lock(const std::type_index& t)
        {
            if(const auto it = locks_.find(t); it == locks_.end())
            {
                locks_.emplace(t);
            }
            else
            {
                throw exceptions::lock_error{ t.name() };
            }
        }

        void unlock(const std::type_index& t)
        {
            if(const auto it = locks_.find(t); it != locks_.end())
            {
                locks_.erase(t);
            }
            else
            {
                throw exceptions::unlock_error{ t.name() };
            }
        }

    private:
        std::set<std::type_index> locks_;
    };
}

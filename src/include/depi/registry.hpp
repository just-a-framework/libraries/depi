#pragma once

#include <depi/exceptions/create_error.hpp>
#include <depi/type_list/type_list.hpp>
#include <depi/type_list/find.hpp>
#include <depi/locker.hpp>
#include <depi/registry_fwd.hpp>
#include <depi/shared_registry.hpp>
#include <depi/unique_registry.hpp>

namespace jaf::depi
{
    template <bool Value = false>
    struct deduction_error
    {
        constexpr deduction_error()
        {
            static_assert(Value, "Could not deduce registry!");
        }
    };

    struct registry
        : shared_registry
        , unique_registry
    {
        template<class T>
        struct enabled
        {
            template<class Registry>
            struct type
            {
                inline static constexpr auto value = Registry::template enabled<T>;
            };

            template<class Registry>
            struct tag
            {
                inline static constexpr auto value = std::is_same_v<T, typename Registry::tag>;
            };
        };

        using bases = type_list<shared_registry, unique_registry>;

        template<
            class Tag,
            class Interface,
            class Implementation
        >
        void bind()
        {
            using Registry = find_if_t<enabled<Tag>::template tag, bases>;
            deduction_error<!std::is_same_v<Registry, none>>();
            
            Registry::template bind<Interface, Implementation>();
        }

        template<
            class Tag,
            class Interface,
            class Implementation,
            class Registry = find_if_t<enabled<Tag>::template tag, bases>
        >
        void bind(Implementation i)
        {
            deduction_error<!std::is_same_v<Registry, none>>();

            if constexpr(std::is_invocable_v<Implementation>)
            {
                Registry::template bind<Interface>(std::function{ i });
            }
            else
            {
                Registry::template bind<Interface>(std::move(i));
            }
        }

        template<class T>
        T create()
        {
            using Registry = find_if_t<enabled<T>::template type, bases>;
            deduction_error<!std::is_same_v<Registry, none>>();

            const auto g = l_.lock<T>();
            return Registry::template create<T>(*this);
        }

        template<
            class T,
            class Registry = find_if_t<enabled<T>::template type, bases>,
            std::enable_if_t<!std::is_same_v<Registry, none>, bool> = true
        >
        operator T()
        {
            return create<T>();
        }

        template<
            class T,
            class Registry = find_if_t<enabled<T>::template type, bases>,
            std::enable_if_t<std::is_same_v<Registry, none>, bool> = true
        >
        operator T()
        {
            throw exceptions::create_error{std::type_index{typeid(T)}.name()};
        }

    private:
        locker l_;
    };
}

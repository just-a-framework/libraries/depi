#pragma once

namespace jaf::depi::exceptions
{
    struct lock_error
        : std::runtime_error
    {
        lock_error(const std::string& name)
            : std::runtime_error{ name + " was already locked!" }
        {
        }
    };
}

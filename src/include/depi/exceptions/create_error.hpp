#pragma once

namespace jaf::depi::exceptions
{
    struct create_error
        : std::runtime_error
    {
        create_error(const std::string_view& name)
            : std::runtime_error{ std::string{"Cannot create object of type "} + name.data() + "!" }
        {
        }
    };
}

#pragma once

namespace jaf::depi::exceptions
{
    struct unlock_error
        : std::runtime_error
    {
        unlock_error(const std::string& name)
            : std::runtime_error{ name + " was not locked!" }
        {
        }
    };
}

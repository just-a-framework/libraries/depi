#pragma once

namespace jaf::depi::exceptions
{
    struct bind_error
        : std::runtime_error
    {
        bind_error(const std::string_view& name)
            : std::runtime_error{ std::string{"Creator already bound for "} + name.data() + "!" }
        {
        }
    };
}

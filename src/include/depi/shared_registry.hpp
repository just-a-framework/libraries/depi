#pragma once

#include <depi/exceptions/bind_error.hpp>
#include <depi/exceptions/create_error.hpp>
#include <depi/details/is_instance_of.hpp>
#include <depi/details/creator.hpp>
#include <depi/registry_fwd.hpp>

namespace jaf::depi
{
    struct shared_registry
    {
        template<class T>
        inline static constexpr bool enabled = details::is_instance_of<std::shared_ptr, T>::value;

        struct tag;

        template<class T>
        struct wrapper
        {
            using type = std::shared_ptr<T>;

            template<class... Args>
            static type make(Args&&... args)
            {
                return std::make_shared<T>(std::forward<Args>(args)...);
            }
        };

        template<class T>
        using creator = details::creator<T, wrapper>;

        template<
            class Interface,
            class Implementation
        >
        void bind()
        {
            try
            {
                bind_internal<Implementation>(typeid(Implementation));
            }
            catch (const exceptions::bind_error&)
            {
                // due to aliasing, this is totally fine.
            }
            bind_internal<Interface, Implementation>();
        }

        template<
            class Interface,
            class Implementation
        >
        void bind(std::shared_ptr<Implementation> ptr)
        {      
            try
            {      
                bind_internal(typeid(Implementation), ptr);
            }
            catch (const exceptions::bind_error&)
            {
                // due to aliasing, this is totally fine.
            }
            bind_internal<Interface, Implementation>();
        }

        template<
            class Interface,
            class Implementation,
            class... Args
        >
        void bind(std::function<std::shared_ptr<Implementation>(Args...)> func)
        {
            try
            {
                bind_internal(typeid(Implementation), func);
            }
            catch (const exceptions::bind_error&)
            {
                // due to aliasing, this is totally fine.
            }
            bind_internal<Interface, Implementation>();
        }

        template<
            class T,
            class = std::enable_if_t<enabled<T>>
        >
        T create(registry& r)
        {
            using real_type = typename T::element_type;

            const auto ind = std::type_index{typeid(real_type)};

            if(auto sit = services_.find(ind); sit != services_.end())
            {
                return std::any_cast<T>(sit->second);
            }
            else
            {
                if(auto cit = creators_.find(ind); cit != creators_.end())
                {
                    auto ptr = cit->second(r);
                    services_.emplace(ind, ptr);
                    return std::any_cast<T>(ptr);
                }
                else
                {
                    if constexpr(!std::is_abstract_v<real_type>)
                    {
                        return creator<real_type>::make(r);
                    }
                    else
                    {
                        throw exceptions::create_error{ind.name()};
                    }
                }
            }
        }

    private:
        using creator_func = std::function<std::any(registry&)>;

        std::map<std::type_index, std::any> services_;
        std::map<std::type_index, creator_func> creators_;

        void check_creators(const std::type_index& ind)
        {
            if(const auto cit = creators_.find(ind); cit != creators_.end())
            {
                throw exceptions::bind_error{ind.name()};
            }
        }

        template<
            class Interface,
            class Implementation
        >
        void bind_internal()
        {
            const auto ind = std::type_index{typeid(Interface)};
            check_creators(ind);

            creators_.emplace(ind, [](auto& r)
            {
                const auto ptr = r.template create<std::shared_ptr<Implementation>>();
                return std::any{ std::static_pointer_cast<Interface>(ptr) };
            });
        }

        template<
            class Implementation
        >
        void bind_internal(const std::type_index& ind)
        {
            check_creators(ind);
            
            // if it's abstract, a binding should take place beforehand
            if constexpr (!std::is_abstract_v<Implementation>)
            {
                creators_.emplace(ind, [](auto& r)
                {
                    return std::any{ creator<Implementation>::make(r) };
                });
            }
        }

        template<
            class Implementation
        >
        void bind_internal(const std::type_index& ind, std::shared_ptr<Implementation> impl)
        {
            check_creators(ind);
            
            creators_.emplace(ind, [ ptr{std::move(impl)} ](auto&)
            {
                return std::any{ ptr };
            });
        }

        template<
            class Implementation,
            class... Args
        >
        void bind_internal(const std::type_index& ind, std::function<std::shared_ptr<Implementation>(Args...)> func)
        {
            check_creators(ind);

            creators_.emplace(ind, [ f{std::move(func)} ](auto& r)
            {
                return std::any{ creator<Implementation>::make(r, f) };
            });
        }
    };
}

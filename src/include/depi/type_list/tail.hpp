#pragma once

#include <depi/type_list/asserts/empty_type_list.hpp>
#include <depi/type_list/asserts/not_type_list.hpp>
#include <depi/type_list/type_list.hpp>

namespace jaf::depi
{
    template<class List>
    struct tail
    {
        using type = decltype(asserts::not_type_list<>());
    };

    template<class Head, class... Tail>
    struct tail<type_list<Head, Tail...>>
    {
        using type = type_list<Tail...>;
    };

    template<>
    struct tail<type_list<>>
    {
        using type = decltype(asserts::empty_type_list<>());
    };

    template<class... Args>
    using tail_t = typename tail<Args...>::type;
}

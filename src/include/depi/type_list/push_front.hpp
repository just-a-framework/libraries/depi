#pragma once

#include <depi/type_list/asserts/not_type_list.hpp>
#include <depi/type_list/type_list.hpp>

namespace jaf::depi
{
    template<class New, class List>
    struct push_front
    {
        using type = decltype(asserts::not_type_list<>());
    };

    template<class New, class... Contained>
    struct push_front<New, type_list<Contained...>>
    {
        using type = type_list<New, Contained...>;
    };

    template<class... Args>
    using push_front_t = typename push_front<Args...>::type;
}

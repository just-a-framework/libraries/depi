#pragma once

#include <depi/type_list/type_list.hpp>
#include <depi/type_list/push_back.hpp>

namespace jaf::depi
{
    template<class Elem, std::size_t RepeatCount>
    struct repeat :
        push_back<Elem, typename repeat<Elem, RepeatCount - 1>::type>
    {
    };

    template<class Elem>
    struct repeat<Elem, 0>
    {
        using type = type_list<>;
    };

    template<class Elem, std::size_t RepeatCount>
    using repeat_t = typename repeat<Elem, RepeatCount>::type;
}

#pragma once

#include <depi/type_list/asserts/not_type_list.hpp>
#include <depi/type_list/type_list.hpp>

namespace jaf::depi
{
    template<class New, class List>
    struct push_back
    {
        using type = decltype(asserts::not_type_list<>());
    };

    template<class New, class... Contained>
    struct push_back<New, type_list<Contained...>>
    {
        using type = type_list<Contained..., New>;
    };

    template<class... Args>
    using push_back_t = typename push_back<Args...>::type;
}

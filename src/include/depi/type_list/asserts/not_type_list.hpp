#pragma once

namespace jaf::depi::asserts
{
    template <bool Value = false>
    struct not_type_list
    {
        constexpr not_type_list()
        {
            static_assert(Value, "List parameter must not be an empty \'jaf::depi::type_list\'");
        }
    };
}

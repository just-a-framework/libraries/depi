#pragma once

namespace jaf::depi::asserts
{
    template <bool Value = false>
    struct empty_type_list
    {
        constexpr empty_type_list()
        {
            static_assert(Value, "List parameter must be a \'jaf::depi::type_list\'");
        }
    };
}

#pragma once

#include <depi/type_list/type_list.hpp>
#include <depi/type_list/head.hpp>
#include <depi/type_list/tail.hpp>

namespace jaf::depi
{
    struct none
    {
    };

    template<template<class> class Function, class List>
    struct find_if :
        std::conditional<
            Function<head_t<List>>::value,
            head_t<List>,
            typename find_if<Function, tail_t<List>>::type
        >
    {
    };

    template<template<class> class Function>
    struct find_if<Function, type_list<>>
    {
        using type = none;
    };


    template<template<class> class Function, class List>
    using find_if_t = typename find_if<Function, List>::type;
}

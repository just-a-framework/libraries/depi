#pragma once

#include <depi/type_list/asserts/not_type_list.hpp>
#include <depi/type_list/type_list.hpp>

namespace jaf::depi
{
    template<template<class...> class Fun, class List>
    struct apply
    {
        using type = decltype(asserts::not_type_list<>());
    };

    template<template<class...> class Fun, class... Contained>
    struct apply<Fun, type_list<Contained...>> :
        Fun<Contained...>
    {
    };

    template<template<class...> class Fun, class List>
    using apply_t = typename apply<Fun, List>::type;

    template<template<class...> class Fun, class List>
    inline constexpr auto apply_v = apply<Fun, List>::value;
}

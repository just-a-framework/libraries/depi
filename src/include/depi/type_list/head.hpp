#pragma once

#include <depi/type_list/asserts/empty_type_list.hpp>
#include <depi/type_list/asserts/not_type_list.hpp>
#include <depi/type_list/type_list.hpp>

namespace jaf::depi
{
    template<class List>
    struct head
    {
        using type = decltype(asserts::not_type_list<>());
    };

    template<class Head, class... Tail>
    struct head<type_list<Head, Tail...>>
    {
        using type = Head;
    };

    template<>
    struct head<type_list<>>
    {
        using type = decltype(asserts::empty_type_list<>());
    };

    template<class... Args>
    using head_t = typename head<Args...>::type;
}
